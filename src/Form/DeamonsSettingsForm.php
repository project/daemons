<?php

namespace Drupal\daemons\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DeamonsSettingsForm.
 *
 * @package Drupal\daemons\Form
 */
class DeamonsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'daemons.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'daemons_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('daemons.settings');
    $form['path_drupal_console'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to the executable drupal console'),
      '#default_value' => $config->get('path_drupal_console'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('daemons.settings')
      ->set('path_drupal_console', $form_state->getValue('path_drupal_console'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
